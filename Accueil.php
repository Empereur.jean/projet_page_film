<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<title>Films</title>
		<link rel ="stylesheet" href ="Accueil.css"/>
		<!-- <link href="ressources/index.jpeg">   -->
	</head>
	<body>
		<header>
			<h1>Répertoire universel des films</h1>
		</header>
		<br><br><br><br><br>
		<h2> Recherche de films par titre</h2>
			<form action="recherche.php" method="get">
				Recherche
				<input type="text" name="recherche" placeholder="Recherchez un film" size=50 >
				<input type="submit" value="Rechercher">
			</form>
		<h2>Recherche de films par année</h2>
		<form action="recherche.php" method="get">
			Recherche
			<select name="rechercheA">
				<option value="">  </option>
				<option value="2010-2019">2010-2019</option>
				<option value="2000-2009">2000-2009</option>
				<option value="1990-1099">1990-1999</option>
				<option value="1980-1089">1980-1989</option>
				<option value="1970-1079">1970-1979</option>
				<option value="1960-1069">1960-1969</option>
				<option value="1950-1059">1950-1959</option>
				<option value="1940-1049">1940-1949</option>
				<option value="1930-1039">1930-1939</option>
				<option value="1920-1029">1920-1929</option>
				<option value="1910-1019">1910-1919</option>
				<option value="1900-1009">1900-1909</option>
				<option value="1890-1899">1890-1899</option>
				<option value="1880-1889">1880-1889</option>
				<option value="1870-1879">1870-1879</option>
			</select>
			<input type="submit" value="Rechercher">
		</form>
		<h2>Recherche de films par genre</h2>
		<form action="recherche.php" method="get">
			Recherche
			<ul>
				<li>Bollywooderie  <input name="rb1" type="radio" value="Bollywooderie  "></li>
				<li>Aventure<input name="rb1" type="radio" value="culte ou my(s)tique"></li>
				<li>Dessin animé<input name="rb1" type="radio" value="pour petits et grands enfants"></li>
				<li>Comédie<input name="rb1" type="radio" value="perle de nanard"></li>
				<li>Thriler<input name="rb1" type="radio" value="cadavre(s) dans le(s) placard(s)"></li>
				<li>Science Fiction<input name="rb1" type="radio" value="conte de fées relooké"></li>
				<li>Romantique<input name="rb1" type="radio" value="pas drôle mais beau  "></li>
				<li>Horeur<input name="rb1" type="radio" value="heurs et malheurs à deux"></li>
			</ul>
			<input type="submit" value="Rechercher">
		</form>
		<br>
		<form action="ajout.php" method="post">
		<?php
			//require_once 'fonctions.php';
			//if (!empty($_GET['titre']) and !empty($_GET['titre_francais'])and !empty($_GET['pays'])and !empty($_GET['annee'])and !empty($_GET['rea'])){
				//$ajout=ajout();
			//}

			?>
			<input type="submit" value="Ajouter un Film">
		</form>
		<br>
		<h2>Liste de tous les films</h2>
		<br>
		<?php
		require('connexion_sql.php');

		$connexion=connect_bd();
    $sql="SELECT DISTINCT * from films";
		foreach ($connexion->query($sql) as $row){
		echo"<a target='_blank' rel='noopener noreferrer'
					href='https://fr.wikipedia.org/wiki/".$row['titre_francais']."'><tr>
				 <td>".$row['titre_original']." / </td>
				 <td>".$row['titre_francais']."</td></tr><br/></a>\n";
		}
 ?>
	</body>
</html>
